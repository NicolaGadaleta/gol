package it.uniba.gol;



import static org.junit.Assert.*;

import org.junit.Test;



public class CellTest {

	@Test
	public void cellShoundBeAlive() throws Exception {
		Cell cell = new Cell(0,0,true);
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void CellShouldThrowAnException() throws Exception{
		Cell cell = new Cell(-1,0,true);
		
	}
	
	@Test
	public void CellShouldBeDead() throws Exception{
		Cell cell = new Cell(0,0,true);
		cell.setAlive(false);
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void XShouldBeZero() throws CustomLifeException {
		Cell cell = new Cell(0,0,true);
		assertEquals(cell.getX(),0);
	}
	
	@Test
	public void YShouldBeZero() throws CustomLifeException {
		Cell cell = new Cell(0,0,true);
		assertEquals(cell.getY(),0);
	}
	
	@Test
	public void NumberOfAliveNeighborsShouldBe0()throws Exception{
		Cell[][] cells = new Cell[2][2];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		
		assertEquals(cells[0][0].getNumberOfAliveNeighbords(),0);
		
	}
	
	@Test
	public void SetNumberOfAliveNeighbordsShouldReturn2() throws CustomLifeException {
		Cell[][] cells = new Cell[2][2];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[0][0].setNumberOfAliveNeighbors(cells);
		
		assertEquals(cells[0][0].getNumberOfAliveNeighbords(),2);
	}
	

	
	@Test
	public void CellShouldSurvive() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[0][0].willSurvive());
		
	}
	
	@Test
	public void CellShouldNotSurviveButIsAlive() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willSurvive());
		
	}
	
	@Test
	public void CellShouldNotSurviveButIsDead() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willSurvive());
		
	}
	
	@Test
	public void CellShouldDie() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[0][0].willDie());
		
	}
	

	
	@Test
	public void CellShouldNotDieButIsAlive() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willDie());
	
	}
	
	@Test
	public void CellShouldNotDieButIsDead() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willDie());
		
	}
	
	
	@Test
	public void CellShouldRevive() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[0][0].willRevive());
		
		
	}
	
	
	@Test
	public void CellShouldNotReviveButIsAlive() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willRevive());
		
		
	}
	
	@Test
	public void CellShouldNotReviveButHasNotEnoughNeighbourds() throws CustomLifeException {
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][0].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[0][0].willRevive());
		
		
	}
	
	

}
